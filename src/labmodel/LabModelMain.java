package labmodel;

public class LabModelMain {
	/**
	 * Generate R, R descriptor and S descriptor
	 */
	public LabModelMain(){
		RCreateMain r = new RCreateMain();
		r.createR();
		r.writeOnDisk();
		
		@SuppressWarnings("unused")
		RelationDescriptor<String> descriptorR =
				new RelationDescriptor<String>(
						String.class, "R");
		@SuppressWarnings("unused")
		RelationDescriptor<String> descriptorS =
				new RelationDescriptor<String>(
						String.class, "S");
	}
	public static void main(String[] args){
		new LabModelMain();
		
		// To display content R
		/*for(int line=0; line<r.length; line++){
			System.out.print(r[line]+" ");
		}
		System.out.println();*/
		// To display R size
		// System.out.println(r.length);
	}
}
