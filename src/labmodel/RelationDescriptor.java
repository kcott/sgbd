package labmodel;

import java.io.File;
import java.lang.reflect.Array;

import system.ReaderFile;
import system.WriterFile;
import system.Configurator;

public class RelationDescriptor<T> {
	// Manage descriptor
	private static int DEFAULT_SIZE =
			Integer.parseInt(
					Configurator.getInstance().getConf("tableSize"));
	private final Class<T> typeDescriptor;
	private T[] descriptors;
	private int sizeMax;
	private int size;
	private int nFileDescriptor;
	private String repertoryRelation;
	
	// Keep relation in memory
	private String relation;

	@SuppressWarnings("unchecked")
	public RelationDescriptor(
			Class<T> typeDescriptor,
			String relation,
			String repertoryRelation){
		// Manage descriptor
		this.sizeMax = DEFAULT_SIZE;
		this.typeDescriptor = typeDescriptor;
		this.descriptors = (T[]) Array.newInstance(this.typeDescriptor, sizeMax);
		this.size = 0;
		this.repertoryRelation = repertoryRelation;

		// Keep relation in memory
		this.relation = relation;
		
		// Create table descriptors
		File relationFile = new File(
				repertoryRelation
				+relation);
		relationFile.listFiles();
		
		int nLoop;
		if(relationFile.listFiles() != null){
			nLoop = relationFile.listFiles().length / DEFAULT_SIZE;
			if(relationFile.listFiles().length % DEFAULT_SIZE != 0)
				nLoop++;
		}else{
			nLoop = 0;
		}
		nFileDescriptor = nLoop;
		
		File rRelation = new File(
				Configurator.getInstance().getConf("repertoryDescriptors")+relation);
		if(rRelation.exists()){
			for(File f:rRelation.listFiles()){
				f.delete();
			}
			rRelation.delete();
		}
		rRelation.mkdirs();
		for(int nFile=0; nFile<nLoop; nFile++){
			int min = nFile*DEFAULT_SIZE;
			int max = Math.min(
					DEFAULT_SIZE*(nFile+1),
					relationFile.listFiles().length);
			String[] fileDescriptor = new String[max-min];
			for(int i=min; i<max; i++){
				fileDescriptor[i-min] = relationFile.listFiles()[i].getName();
			}
			try {
				WriterFile.write(
						Configurator.getInstance().getConf("repertoryDescriptors")+relation+"/"+nFile,
						fileDescriptor);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	@SuppressWarnings("unchecked")
	public RelationDescriptor(
			Class<T> typeDescriptor,
			String relation){
		// Manage descriptor
		this.sizeMax = DEFAULT_SIZE;
		this.typeDescriptor = typeDescriptor;
		this.descriptors = (T[]) Array.newInstance(this.typeDescriptor, sizeMax);
		this.size = 0;
		if(this.repertoryRelation == null)
			this.repertoryRelation = Configurator.getInstance().getConf("repertoryRelations");

		// Keep relation in memory
		this.relation = relation;
		
		// Create table descriptors
		File relationFile = new File(
				repertoryRelation
				+relation);
		relationFile.listFiles();
		
		int nLoop;
		if(relationFile.listFiles() != null){
			nLoop = relationFile.listFiles().length / DEFAULT_SIZE;
			if(relationFile.listFiles().length % DEFAULT_SIZE != 0)
				nLoop++;
		}else{
			nLoop = 0;
		}
		nFileDescriptor = nLoop;
		
		File repertoryRelation = new File(
				Configurator.getInstance().getConf("repertoryDescriptors")+relation);
		if(repertoryRelation.exists()){
			for(File f:repertoryRelation.listFiles()){
				f.delete();
			}
			repertoryRelation.delete();
		}
		repertoryRelation.mkdirs();
		for(int nFile=0; nFile<nLoop; nFile++){
			int min = nFile*DEFAULT_SIZE;
			int max = Math.min(
					DEFAULT_SIZE*(nFile+1),
					relationFile.listFiles().length);
			String[] fileDescriptor = new String[max-min];
			for(int i=min; i<max; i++){
				fileDescriptor[i-min] = relationFile.listFiles()[i].getName();
			}
			try {
				WriterFile.write(
						Configurator.getInstance().getConf("repertoryDescriptors")+relation+"/"+nFile,
						fileDescriptor);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	/**
	 * Get the size of the relation
	 * @return int
	 */
	public int getSize(){
		return size;
	}
	/**
	 * Get the relation repertory
	 * @return String
	 */
	public String getRepertoryRelation(){
		return repertoryRelation;
	}
	/**
	 * Get the relation name
	 * @return String
	 */
	public String getRelationName(){
		return relation;
	}
	/**
	 * Get a descriptor by the index
	 * @param index int
	 * @return T
	 */
	public T getDescriptor(int index){
		if(0<=index && index<size)
			return descriptors[index];
		throw new IllegalArgumentException();
	}
	/**
	 * Add a descriptor<br/>
	 * Raise an exception if the size of the table descriptor is reach
	 * @param index T
	 * @param descriptor U
	 */
	public void addDescriptor(int index, T descriptor){
		if(0<=size && size<getSizeMax()){
			descriptors[size] = descriptor;
			size++;
		}else
			throw new IllegalArgumentException();
	}
	/**
	 * Return the size max of the table descriptor
	 * @return int
	 */
	public int getSizeMax(){
		return sizeMax;
	}
	/**
	 * Return the file descriptor number
	 * @return
	 */
	public int getNFileDescriptor(){
		return nFileDescriptor;
	}
	@SuppressWarnings("unchecked")
	public void loadFileDescriptor(int index){
		if(index<0 || nFileDescriptor<=index)
			throw new IllegalArgumentException();
		try {
			clear();
			Object[] objects = ReaderFile.read(
					Configurator.getInstance().getConf("repertoryDescriptors")+
					relation+"/"+index);
			try{
				for(Object o:objects){
					addDescriptor(size, (T) o);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void clear(){
		size = 0;
		for(@SuppressWarnings("unused") T d:descriptors)
			d = null;
	}
}
