package labmodel;

import java.io.File;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import join.memory.NestedLoop;
import system.WriterFile;
import system.Configurator;
import system.SystemConfiguration;

public class RCreateMain {
	private List<String> r;
	public RCreateMain(){
		r = new ArrayList<String>();
	}
	/**
	 * Create R table and shuffle it
	 * A-Z and A-H
	 * @return
	 */
	public String[] createR(){
		if(r != null)
			r.clear();
		Character start1 = 'A';
		Character end1 = 'Z';
		Character start2 = 'A';
		Character end2 = 'H';
		
		//int size = (((int)end1)-((int)start1)+1)*(((int)end2)-((int)start2)+1);
		
		Character begin1 = start1;
		Character begin2 = start2;
		
		while( ((int)begin1) != (((int)end1)+1) ){
			while( ((int)begin2) != (((int)end2)+1) ){
				r.add(begin1.toString()+begin2.toString());
				begin2 = (char)(((int)begin2) +1);
			}
			begin2 = start2;
			begin1 = (char)(((int)begin1) +1);
		}
		Collections.shuffle(r);
		return getR();
	}
	/**
	 * Return size of socked R table
	 * @return
	 */
	public int getSize(){
		return r.size();
	}
	/**
	 * Return R table
	 * @return
	 */
	public String[] getR(){
		return r.toArray(new String[r.size()]);
	}
	/**
	 * Write R table on disk
	 * @param folder String
	 */
	public void writeOnDisk(){
		NestedLoop<String> data = new NestedLoop<String>(String.class);
		int nFiles = (int) Math.ceil((getSize()*1.) / (data.getSize()));
		
		File repertory = new File(
				Configurator.getInstance().getConf("repertoryRelations")+"R");
		if(repertory.exists()){
			for(File f:repertory.listFiles()){
				f.delete();
			}
			repertory.delete();
		}
		repertory.mkdirs();
		
		for(int n=0; n<nFiles; n++){
			try {
				// Get the min and the max to current file
				int min = n*data.getSize();
				int max = Math.min((n+1)*data.getSize()-1, getSize()-1);
				
				// Get datas
				String[] datas = new String[max+1-min];
				int counter = 0;
				for(int line=min; line<=max; line++){
					datas[counter] = r.get(line);
					counter++;
				}
				
				// Write on file
				String file = SystemConfiguration.getInstance().fileRelation("R", "R"+n);
				WriterFile.write(file ,datas);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
