package join.memory;

import java.lang.reflect.Array;
import java.util.Arrays;

import join.Join;
import system.Index;
import system.Configurator;

public class NestedLoop<T> implements Join<T>{
	/**
	 * Size by default to relation
	 */
	protected final int DEFAULT_SIZE =
			Integer.parseInt(
					Configurator.getInstance().getConf("tableSize"));
	/**
	 * Type of T class
	 */
	private final Class<T> type;
	/**
	 * Size to relation
	 */
	private int size;
	/**
	 * Construct a data with a array
	 * @param type Class<T>
	 */
	public NestedLoop(Class<T> type){
		super();
		this.type = type;
		this.size = DEFAULT_SIZE;
	}
	/**
	 * Construct a data with a array and a size
	 * @param type Class<T>
	 * @param size int
	 */
	public NestedLoop(Class<T> type, int size){
		super();
		this.type = type;
		this.size = size;
	}
	/**
	 * Join two table of char by a nested loop in one table<br/>
	 * Stop at the max size table defined
	 * @param relation1 T[]
	 * @param relation2 T[]
	 * @return T[]
	 */
	public T[] join(T[] relation1, T[] relation2){
		@SuppressWarnings("unchecked")
		T[] c = (T[]) Array.newInstance(type, getSize());
		int size_c = 0;
		for(int i1=0; i1<relation1.length && size_c<getSize(); i1++){
			for(int i2=0; i2<relation2.length && size_c<getSize(); i2++){
				if(relation1[i1].equals(relation2[i2])){
					c[size_c] = relation1[i1];
					size_c++;
				}
			}
		}
		c = Arrays.copyOf(c, size_c);
		return c;
	}
	/**
	 * Get the size of the relation
	 * @return int
	 */
	public int getSize(){
		return size;
	}
}
