package join.memory;

import java.lang.reflect.Array;
import java.util.Arrays;

import join.Join;
import system.Index;
import system.Configurator;

public class MergeJoin<T> implements Join<T>{
	/**
	 * Size by default to relation
	 */
	protected final int DEFAULT_SIZE =
			Integer.parseInt(
					Configurator.getInstance().getConf("tableSize"));
	/**
	 * Type of T class
	 */
	private final Class<T> type;
	/**
	 * Size to relation
	 */
	private int size;
	/**
	 * Construct a data with a array
	 * @param type Class<T>
	 */
	public MergeJoin(Class<T> type){
		super();
		this.type = type;
		this.size = DEFAULT_SIZE;
	}
	/**
	 * Construct a data with a array and a size
	 * @param type Class<T>
	 * @param size int
	 */
	public MergeJoin(Class<T> type, int size){
		super();
		this.type = type;
		this.size = size;
	}
	/**
	 * Join two table of char by a merge in one table<br/>
	 * Stop at the max size table defined
	 * @param table1 T[]
	 * @param table2 T[]
	 * @return T[]
	 */
	public T[] join(T[] relation1, T[] relation2){
		@SuppressWarnings("unchecked")
		T[] c = (T[]) Array.newInstance(type, getSize());
		int size_c = 0;
		int i1 = 0;
		int i2 = 0;
		while(i1<relation1.length && i2<relation2.length){
			//if(table1[i1] < table2[i2]){
			if(Index.objectToInt(relation1[i1]) < Index.objectToInt(relation2[i2])){
				i1++;
			}else if(relation1[i1].equals(relation2[i2])){
				int temp = i2;
				do{
					i2 = temp;
					do{
						c[size_c++] = relation1[i1];
						if(getSize() == size_c)
							return c;
						i2++;
					}while ((i2<relation2.length) &&
							(relation1[i1].equals(relation2[i2])));
					i1++;
				}while ((i1<relation1.length) &&
						(relation1[i1].equals(relation2[temp])));
			}else{
				i2++;
			}
		}
		c = Arrays.copyOf(c, size_c);
		return c;
	}
	/**
	 * Get the size of the relation
	 * @return int
	 */
	public int getSize(){
		return size;
	}
}
