package join.disk;

import java.io.File;

import labmodel.RelationDescriptor;
import system.ReaderFile;
import system.WriterFile;
import system.Configurator;
import system.SystemConfiguration;

public class DiskNestedLoop<T> {
	private final Class<T> type;
	private final int DEFAULT_SIZE = 
			Integer.parseInt(
					Configurator.getInstance().getConf("tableSize"));
	private int size;
	/**
	 * Create the class with T class type
	 * @param type
	 */
	public DiskNestedLoop(Class<T> type){
		this.type = type;
		this.size = DEFAULT_SIZE;
	}
	/**
	 * Create the class with T class type and a specified size in memory
	 * @param type class<T>
	 * @param size int
	 */
	public DiskNestedLoop(Class<T> type, int size){
		this.type = type;
		this.size = size;
	}
	/**
	 * Return the size in memory
	 * @return int
	 */
	public int getSize(){
		return this.size;
	}
	public void joinNestedLoop(String relationName1, String relationName2, String relationNameResult){
		// Creation du repertoire sortie
		File repertoryResult = new File(SystemConfiguration.getInstance().pathSelect(relationNameResult));
		if(repertoryResult.exists()){
			for(File f:repertoryResult.listFiles())
				f.delete();
			repertoryResult.delete();
		}
		repertoryResult.mkdirs();
		
		// Creation des descripteurs de relations
		RelationDescriptor<String> descriptor1 =
				new RelationDescriptor<String>(
						String.class, relationName1);
		RelationDescriptor<String> descriptor2 =
				new RelationDescriptor<String>(
						String.class, relationName2);
		
		RelationDescriptor<String> descriptorResult =
				new RelationDescriptor<String>(
						String.class, relationNameResult,
						Configurator.getInstance().getConf("repertorySelects"));
		
		// Tableau de sauvegarde
		Object[] results = new Object[getSize()];
		int sizeResult = 0;
		int nResultFile = 0;
		
		// Jointure sur disque
		int nDescriptor1 = 0;
		int nDescriptor2 = 0; int incDescriptor2 = +1;
		if(0<descriptor2.getNFileDescriptor())
			descriptor2.loadFileDescriptor(0);
		/*
		 * 4 boucles -> 2 descripteurs et 2 parcours de fichier
		 */
		for(nDescriptor1=0; nDescriptor1<descriptor1.getNFileDescriptor(); nDescriptor1++){
			descriptor1.loadFileDescriptor(nDescriptor1);
			while(0<=nDescriptor2 && nDescriptor2<descriptor2.getNFileDescriptor()){
				for(int d1=0; d1<descriptor1.getSize(); d1++){
					for(int d2=0; d2<descriptor2.getSize(); d2++){
						/*
						 * Jointure en m�moire
						 */
						Object[] table1;
						Object[] table2;
						try {
							table1 = ReaderFile.read(
											descriptor1.getRepertoryRelation()+
											descriptor1.getRelationName()+"/"+
											descriptor1.getDescriptor(d1));
							table2 = ReaderFile.read(
											descriptor2.getRepertoryRelation()+
											descriptor2.getRelationName()+"/"+
											descriptor2.getDescriptor(d2));
							for(int i1=0; i1<table1.length; i1++){
								for(int i2=0; i2<table2.length; i2++){
									if(table1[i1].equals(table2[i2])){
										results[sizeResult] = table1[i1];
										sizeResult++;
										if(sizeResult == Integer.parseInt(Configurator.getInstance().getConf("tableSize"))){
											writeOnDisk(descriptorResult, results, nResultFile);
											nResultFile++;
											sizeResult = 0;
										}
									}
								}
							}
						}catch(Exception e){
							e.printStackTrace();
						}
					}
				}
				
				if(descriptor2.getNFileDescriptor() != 1){
					nDescriptor2 += incDescriptor2;
					if(nDescriptor2 == 0)
						incDescriptor2 = +1;
					else if(nDescriptor2 == descriptor2.getSize())
						incDescriptor2 = -1;
					descriptor2.loadFileDescriptor(nDescriptor2);
				}else{
					break;
				}
			}
		}
		if(0<sizeResult){
			results[sizeResult] = null;
			writeOnDisk(descriptorResult, results, nResultFile);
			nResultFile++;
		}
	}
	private void writeOnDisk(RelationDescriptor<String> descriptor, Object[] objects, int indexFile){
		try{
			WriterFile.write(
					descriptor.getRepertoryRelation()+
						descriptor.getRelationName()+"/"+
						descriptor.getRelationName()+
						indexFile+
						Configurator.getInstance().getConf("relationFileExtention"),
					objects);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
