package join.disk;

import join.Join;
import system.Configurator;

public class DiskMergeJoin<T> implements Join<T>{
	private final Class<T> type;
	private final int DEFAULT_SIZE = 
			Integer.parseInt(
					Configurator.getInstance().getConf("tableSize"));
	private int size;
	/**
	 * Create the class with T class type
	 * @param type
	 */
	public DiskMergeJoin(Class<T> type){
		this.type = type;
		this.size = DEFAULT_SIZE;
	}
	/**
	 * Create the class with T class type and a specified size in memory
	 * @param type class<T>
	 * @param size int
	 */
	public DiskMergeJoin(Class<T> type, int size){
		this.type = type;
		this.size = size;
	}
	@Override
	public T[] join(T[] relation1, T[] relation2) {
		return null;
	}
}
