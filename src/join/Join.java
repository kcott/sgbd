package join;

public interface Join<T> {
	public T[] join(T[] relation1, T[] relation2);
}
