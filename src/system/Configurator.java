package system;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class Configurator {
	// Singleton
	private static Configurator conf;
	public static Configurator getInstance(){
		if(conf == null)
			conf = new Configurator();
		return conf;
	}
	
	// Class
	private Map<String,String> confs;
	private Configurator(){
		confs = new HashMap<String,String>();
		FileInputStream fis;
		try {
			fis = new FileInputStream("database.conf");
			BufferedReader br = new BufferedReader(new InputStreamReader(fis));
			String line;
			while((line = br.readLine()) != null){
				String params[] = line.split(" ");
				confs.put(params[0], params[1]);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
	/**
	 * Get the configuration of the key
	 * @param key
	 * @return
	 */
	public String getConf(String key){
		return confs.get(key);
	}
}
