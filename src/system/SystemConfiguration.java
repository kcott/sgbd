package system;

public class SystemConfiguration {
	/*
	 * Singleton
	 */
	private SystemConfiguration(){}
	private static SystemConfiguration system;
	public static SystemConfiguration getInstance(){
		if(system == null)
			system = new SystemConfiguration();
		return system;
	}
	/*
	 * Real class
	 */
	public String pathConf(String conf){
		return Configurator.getInstance().getConf("repertoryConfs")
				+conf+"/";
	}
	public String fileConf(String conf, String file){
		return Configurator.getInstance().getConf("repertoryConfs")
				+conf+"/"+file
				+getConfExtention();
	}
	public String pathDescriptor(String descriptor){
		return Configurator.getInstance().getConf("repertoryDescriptors")
				+descriptor+"/";
	}
	public String fileDescriptor(String descriptor, String file){
		return Configurator.getInstance().getConf("repertoryDescriptors")
				+descriptor+"/"+file
				+getFileExtention();
	}
	public String pathRelation(String relation){
		return Configurator.getInstance().getConf("repertoryRelations")
				+relation+"/";
	}
	public String fileRelation(String relation, String file){
		return Configurator.getInstance().getConf("repertoryRelations")
				+relation+"/"+file
				+getFileExtention();
	}
	public String pathSelect(String select){
		return Configurator.getInstance().getConf("repertorySelects")
				+select+"/";
	}
	public String fileSelect(String select, String file){
		return Configurator.getInstance().getConf("repertorySelects")
				+select+"/"+file
				+getFileExtention();
	}
	public String getFileExtention(){
		return Configurator.getInstance().getConf("relationFileExtention");
	}
	public String getConfExtention(){
		return Configurator.getInstance().getConf("confFileExtention");
	}
}
