package system;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public abstract class ReaderFile {
	/**
	 * 
	 * @param pathfile String who contain the path to the file data
	 * @return Object[] table of read data
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static Object[] read(String pathfile) throws FileNotFoundException, IOException{
		FileInputStream fis = new FileInputStream(pathfile);
		BufferedReader br = new BufferedReader(new InputStreamReader(fis));
		String line; // values in current line
		int nLines = 0;
		while((line = br.readLine()) != null){
			nLines++;
		}
		Object[] cs = new Object[nLines];
		nLines = 0;
		fis.getChannel().position(0);
		while((line = br.readLine()) != null){
			cs[nLines] = line;
			nLines++;
		}
		br.close();
		fis.close();
		return cs;
	}
	/**
	 * Read a BufferedReader who contains data with a limit to the size of the return<br/>
	 * Use to fragmented data
	 * @param bufferedReader BufferedReader who contains data
	 * @return Object[]
	 * @throws IOException
	 */
	public static Object[] read(BufferedReader bufferedReader, int nLines) throws IOException{
		String line; // values in current line
		Object[] cs = new Object[nLines];
		int index = 0;
		while(((line = bufferedReader.readLine()) != null) && (index<nLines)){
			cs[index] = line.charAt(0);
			index++;
		}
		cs = Arrays.copyOf(cs, index);
		return cs;
	}
}
