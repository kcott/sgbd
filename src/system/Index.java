package system;

public abstract class Index {
	/**
	 * Convert an object to an int
	 * @param object
	 * @return int
	 */
	public static int objectToInt(Object object){
		return Integer.valueOf(object.toString());
	}
}
