package system;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

public abstract class WriterFile {
	/**
	 * Write in given file the given array of chars<br/>
	 * Replace the previous file if he exists
	 * @param pathfile String
	 * @param array Object[]
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void write(String pathfile, Object[] array) throws FileNotFoundException, IOException{
		PrintWriter pw = new PrintWriter(pathfile, "UTF-8");
		for(int i=0; i<array.length; i++){
			if(array[i] == null)
				break;
			pw.println(array[i].toString());
		}
		pw.close();
	}
	/**
	 * Write in given file the given array of chars with a limit to the size of the return<br/>
	 * Replace the previous file if he exists<br/>
	 * Use to fragmented data
	 * @param pathfile String
	 * @param arrayChars Object[]
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void write(String pathfile, Object[] array, int nLines) throws FileNotFoundException, IOException{
		PrintWriter pw = new PrintWriter(pathfile, "UTF-8");
		for(int i=0; i<array.length && i<nLines; i++){
			pw.println(array[i].toString());
		}
		pw.close();
	}
}
